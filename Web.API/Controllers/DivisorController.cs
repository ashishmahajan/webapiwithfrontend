﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Controllers
{
    [Route("api/[controller]")]
    public class DivisorController : Controller
    {
        /// <summary>
        /// Given the set [1, 1, 1, 3, 5, 9] complete the sequence to 11 numbers
        /// Get the nTH instance of the sequence where (n / x) has no remainder
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nth">the nth count</param>
        /// <returns></returns>
        [HttpGet]
        public int Get(int x, int nth)
        {
            int result = 0;
            // Array numArray = new Array { };
            try
            {
                // Call function to get the Output of Series till 11 number  ["1, 1, 1, 3, 5, 9, 17, 31, 57, 105, 193"]
                string resultSeries = GetSeriesTillElevenNumber();

                bool isComplete = true;
                int count = 0;

                int seriesNextValue = 0;
                string strSeries = "1, 1, 1";
                string[] numArrayNew = strSeries.Split(",");
                int reminder = 0;
                do
                {
                    numArrayNew = strSeries.Split(",");

                    // Getting the value from array
                    var value1 = numArrayNew[numArrayNew.Length - 1];

                    var value2 = numArrayNew[numArrayNew.Length - 2];

                    var value3 = numArrayNew[numArrayNew.Length - 3];

                    seriesNextValue = Convert.ToInt16(value1) + Convert.ToInt16(value2) + Convert.ToInt16(value3);

                    strSeries = strSeries + ", " + seriesNextValue;

                    count++;

                    reminder = seriesNextValue % x;

                    if (reminder == 0)
                    {
                        result = seriesNextValue;
                    }

                    if (count == nth)
                    {
                        isComplete = false;
                        break;
                    }

                } while (isComplete);

            }
            catch (Exception)
            {

            }

            return result;
        }

        /// <summary>
        /// Function to get the series till 11 number
        /// </summary>
        /// <returns></returns>
        public string GetSeriesTillElevenNumber()
        {
            int sequenceLength = 8;
            int seriesNextValue = 0;
            string strSeries = "1, 1, 1";
            string[] numArrayNew = strSeries.Split(",");
            // Array numArray = new Array { };
            try
            {
                for (int i = 0; i < sequenceLength; i++)
                {
                    numArrayNew = strSeries.Split(",");
                    // Find index of element with value 5.
                    var value1 = numArrayNew[numArrayNew.Length - 1];

                    // Find index of value 3.
                    var value2 = numArrayNew[numArrayNew.Length - 2];

                    // Find last index of 5.
                    var value3 = numArrayNew[numArrayNew.Length - 3];

                    seriesNextValue = Convert.ToInt16(value1) + Convert.ToInt16(value2) + Convert.ToInt16(value3);

                    strSeries = strSeries + ", " + seriesNextValue;
                }

                // Series till 11 number
                strSeries = strSeries.TrimEnd(',');
            }
            catch (Exception)
            {

            }

            return strSeries;
        }
    }
}
