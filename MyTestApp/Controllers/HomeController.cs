﻿using MyTestApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MyTestApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {      
            return View();
        }
        [HttpPost]
        public ActionResult Index(HomeModel objHomeModel)
        {
            int x = objHomeModel.Xnumber;
            int nth = objHomeModel.NthNumber;
            string url = "http://localhost:60409/api/Divisor?x=" + x + "&nth=" + nth + " ";

            HttpClient webClient = new HttpClient();
            Uri uri = new Uri(url);
            HttpResponseMessage response = webClient.GetAsync(uri).Result;
            var jsonString = response.Content.ReadAsStringAsync().Result;
            var objData = JsonConvert.DeserializeObject(jsonString);

            return RedirectToAction("About",new { resultData= objData });
        }

        public ActionResult About(string resultData)
        {
            ViewBag.Result = resultData;
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}